:- begin_tests(ansi_termx).
:- use_module(prolog/ansi_termx).

test(colors) :-
  with_output_to(codes(Result),
      forall(color(Color),
        ( format(atom(Text), '~w text~n', Color),
          color(Color, default, [default], Text, TextOut),
          write(TextOut)
        )
      )),
  read_file_to_codes('tests/colors_test_output.txt', Result, []).

test(background) :-
  with_output_to(codes(Result),
      forall(color(Color),
        ( format(atom(Text), '~w text~n', Color),
          color(default, Color, [default], Text, TextOut),
          write(TextOut)
        )
      )),
  read_file_to_codes('tests/background_test_output.txt', Result, []).

test(styles) :-
  with_output_to(codes(Result),
      forall(style(Style),
        ( format(atom(Text), '~w text~n', Style),
          color(default, default, [Style], Text, TextOut),
          write(TextOut)
        )
      )),
  read_file_to_codes('tests/style_test_output.txt', Result, []).

test(cursor_save) :-
  with_output_to(codes(Result),
      ( write(test0),
        cursor_save,
        cursor_position(_, 40),
        write(test1),
        cursor_unsave,
        writeln(test2))),
  read_file_to_codes('tests/cursor_save_test_output.txt', Result, []).

test(clear_screen) :-
  with_output_to(codes(Result),
      ansi_termx:clear_screen),
  read_file_to_codes('tests/clear_screen_test_output.txt', Result, []).

test(reset) :-
  with_output_to(codes(Result),
      ansi_termx:clear_screen),
  read_file_to_codes('tests/reset_test_output.txt', Result, []).

test(set_tab) :-
  with_output_to(codes(Result),
      ( set_tab,
        clear_tab,
        set_tab,
        clear_all_tabs
      )),
  read_file_to_codes('tests/set_tab_test_output.txt', Result, []).

test(scroll_fail, [fail]) :-
  scroll(1, left).
test(scroll_fail, [fail]) :-
  scroll(a, up).

test(scroll) :-
  with_output_to(codes(Result),
      ( scroll(3, up),
        scroll(3, down)
      )),
  read_file_to_codes('tests/scroll_test_output.txt', Result, []).

test(cursor_move) :-
  with_output_to(codes(Result),
      ( cursor_move(2, up),
        cursor_move(2, down),
        cursor_move(2, backward),
        cursor_move(2, forward)
      )),
  read_file_to_codes('tests/cursor_move_test_output.txt', Result, []).

:- end_tests(ansi_termx).
