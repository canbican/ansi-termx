testfiles   := $(wildcard tests/*.plt)
version     := $(shell swipl -s pack.pl -g 'version(X), writeln(X).' -t halt)
packname    := $(shell basename $$(pwd))

check: $(testfiles)

%.plt: FORCE
	swipl -s $@ -g 'run_tests' -t halt && \
	swipl -q -s $@ -g 'show_coverage(run_tests)' -t halt | \
	grep -E '/prolog/..*\.pl '| \
	awk '{c = ( $$2 * $$3 / 100 ); sum += $$2; covered += c} END {printf "Coverage: %.2f%%\n",covered*100.0/sum}'

release: check make_tgz releasebranch FORCE
	mv -n ../$(packname)-$(version).tgz .
	git add .
	git commit -m "release $(version)"

releasebranch: FORCE
	git checkout releases

make_tgz: FORCE
	rm -f ../$(packname)-$(version).tgz
	find ../$(packname) -name '*.pl' -o -name LICENSE |sed -e 's/^...//'|xargs tar cvzfp ../$(packname)-$(version).tgz -C ..

FORCE:
